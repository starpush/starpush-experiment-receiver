package net.starpush.android.receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("LOOKATME", "received message");
        TestHelper.sendToast(context, "Received message");
        TestHelper.longRunCheckInternet(goAsync());
    }
}
