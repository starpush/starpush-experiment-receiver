package net.starpush.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class TestHelper {

    public static void sendToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void longRunCheckInternet(@Nullable BroadcastReceiver.PendingResult result) {
        Log.e("RECEIVER", "Trying to start extra thread");

        // check internet
        new Thread() {
            @Override
            public void run() {
                Log.e("RECEIVER", "Started inet thread");

                HttpsURLConnection urlConnection = null;
                try {
                    Log.e("I AM TIME", "I am stamp 1");
                    URL url = new URL("https://icanhazip.com/");
                    urlConnection = (HttpsURLConnection) url.openConnection();
                    Log.e("I AM TIME", "I am stamp 1.5");
                    BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder total = new StringBuilder();
                    Log.e("I AM TIME", "I am stamp 1.75");
                    for (String line; (line = in.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    Log.e("I AM TIME", "I am stamp 2");
                    Log.e("GOT INFO", "IP is " + total.toString());
                    Log.e("I AM TIME", "I am stamp 3");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }
        }.start();

        // check running
        new Thread() {
            @Override
            public void run() {
                Log.e("RECEIVER", "Started count thread");
                // check internet available
                // check how long we are allowed to run
                for (int i = 0; i < 9; ++i) {
                    Log.e("I AM TIME", "Thread: " + Thread.currentThread().getId() + ", Seconds: " + (i + 1));

                    try {
                        Log.e("I AM SLEEP", "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result != null) {
                    result.finish();
                }
                Log.e("I AM DONE", "Peace, Counter out!");
            }
        }.start();

    }
}
